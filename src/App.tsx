import { reatomContext } from '@reatom/react';
import React, { useEffect } from 'react';
import { createStore } from '@reatom/core';

import 'antd/dist/antd.css';
import './assets/index.css';
import 'bootstrap-4-grid';
import 'animate.css';

import { Manager } from './Manager';
import { Route, Routes } from 'react-router-dom';
import { Scoreboard } from './screens/Scoreboard';
import { Control } from './screens/Control';

const App = () => {
  const store = createStore();

  return (
    <reatomContext.Provider value={store}>
      <div className="d-flex justify-content-around align-items-center h-100">
        <Routes>
          <Route path={'/'} element={<Manager />} />
          <Route path={'/control/:sessionId'} element={<Control />} />
          <Route path={'/scoreboard'} element={<Scoreboard />} />
        </Routes>
      </div>
    </reatomContext.Provider>
  );
}

export default App;
