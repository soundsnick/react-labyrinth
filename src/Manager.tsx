import React, { FC, useEffect } from 'react';
import { useAtom } from '@reatom/react';
import { Menu } from './screens/Menu';
import { Lost } from './screens/Lost';
import { gameAtom, Stages } from './atoms/gameAtom';
import { Game } from './screens/Game';
import { scoreboardAtom } from './atoms/scoreboardAtom';

export const Manager: FC = () => {
  const [{ stage }] = useAtom(gameAtom);
  const [, { init }] = useAtom(scoreboardAtom);

  const states = {
    [Stages.MENU]: Menu,
    [Stages.STARTED]: Game,
    [Stages.LOST]: Lost,
  }

  const Component = states[stage];

  useEffect(() => {
    init();
  }, [init]);

  return (
    <Component />
  );
};
