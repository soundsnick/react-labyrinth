import styled from '@emotion/styled';
import { blue } from '@ant-design/colors';

export const ControlButton = styled.button`
  width: 80px;
  height: 80px;
  background-color: transparent;
  border: 1px solid ${blue.primary};
  color: ${blue.primary};
  border-radius: 8px;
  transition: background-color 0.2s;
  &:active {
    background-color: ${blue[8]};
  }
`;
