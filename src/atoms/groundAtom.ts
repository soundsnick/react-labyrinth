import { createAtom } from '@reatom/core';

enum Complexity {
  EASY = 16,
  MEDIUM = 32,
  HARD = 40,
  EXPERT = 80
}

type Road = Record<number, Record<number, boolean>>;

type Props = {
  road: Road;
  roadLength: number;
  groundSize: number;
  level: number;
  loading: boolean;
};

const initialState: Props = {
  road: {},
  roadLength: 0,
  groundSize: 12,
  level: 0,
  loading: false,
};

enum actionTypes {
  SET = '_set',
  LOAD_GROUND = 'loadGround',
}

const actionCreators = {
  [actionTypes.LOAD_GROUND]: (level: number) => level,
  [actionTypes.SET]: (newState: Props) => newState,
};

const countNeighbors = (road: Road, position: number[]): number => {
  let count = 0;
  if (road[position[0]-1]?.[position[1]] !== undefined) { // up
    count += 1;
  }
  if (road[position[0]+1]?.[position[1]] !== undefined) { // down
    count += 1;
  }
  if (road[position[0]]?.[position[1]-1] !== undefined) { // left
    count += 1;
  }
  if (road[position[0]]?.[position[1]+1] !== undefined) { // right
    count += 1;
  }

  return count;
}

const loadRoad = async (groundSize: number) => {
  const road: Road = { 0: { 0: false } };
  let roadLength = 0;
  let prev = [0, 0];
  while (true) {
    const newValues = {
      down: [prev[0] - 1, prev[1]],
      up: [prev[0] + 1, prev[1]],
      left: [prev[0], prev[1] - 1],
      right: [prev[0], prev[1] + 1],
    };
    const picks = [
      ...(prev[0] > 0 && !road[newValues.down[0]]?.[newValues.down[1]] && countNeighbors(road, newValues.down) === 1 ? [ newValues.down ]: []),
      ...(prev[0] < groundSize - 1 && !road[newValues.up[0]]?.[newValues.down[1]] && countNeighbors(road, newValues.up) === 1 ? [ newValues.up ]: []),
      ...(prev[1] > 0 && !road[newValues.left[0]]?.[newValues.left[1]] && countNeighbors(road, newValues.left) === 1 ? [ newValues.left ]: []),
      ...(prev[1] < groundSize - 1 && !road[newValues.right[0]]?.[newValues.right[1]] && countNeighbors(road, newValues.right) === 1 ? [ newValues.right ]: []),
    ];
    const randomPick = picks[Math.floor(Math.random() * picks.length)];
    if (randomPick) {
      if (road[randomPick[0]]) {
        road[randomPick[0]][randomPick[1]] = false;
      } else {
        road[randomPick[0]] = {};
        road[randomPick[0]][randomPick[1]] = false;
      }
      prev = randomPick;
      roadLength += 1;
    } else {
      road[prev[0]][prev[1]] = true;
      break;
    }
  }

  return { road, roadLength };
};

export const groundAtom = createAtom(
  actionCreators,
  ({ onAction, schedule, create }, state = initialState) => {
    onAction(actionTypes.SET, (payload) => {
      state = payload;
    });

    onAction(actionTypes.LOAD_GROUND, (newLevel) => {
      schedule((dispatch) => {
        dispatch(create(actionTypes.SET, { ...initialState, loading: true }));

        let groundSize = Complexity.EASY;
        if (newLevel >= 5 && newLevel < 10) {
          groundSize = Complexity.MEDIUM;
        } else if (newLevel >= 10 && newLevel < 15) {
          groundSize = Complexity.HARD;
        } else if (newLevel >= 15) {
          groundSize = Complexity.HARD;
        }

        loadRoad(groundSize).then(({ road, roadLength }) => {
          dispatch(create(actionTypes.SET, { road, roadLength, loading: false, groundSize, level: newLevel }));
        });
      });
    });

    return state;
  });
