import { createAtom } from '@reatom/core';

type Props = {
  position: [number, number];
};

const initialState: Props = {
  position: [0, 0]
};

enum actionTypes {
  SET = '_set',
  RESET = 'reset',
  MOVE_LEFT = 'moveLeft',
  MOVE_RIGHT = 'moveRight',
  MOVE_UP = 'moveUp',
  MOVE_DOWN = 'moveDown',
}

const actionCreators = {
  [actionTypes.SET]: (newState: Props) => newState,
  [actionTypes.RESET]: () => null,
  [actionTypes.MOVE_LEFT]: () => null,
  [actionTypes.MOVE_RIGHT]: (groundSize: number) => groundSize,
  [actionTypes.MOVE_UP]: () => null,
  [actionTypes.MOVE_DOWN]: (groundSize: number) => groundSize,
};

export const playerAtom = createAtom(
  actionCreators,
  ({ onAction, schedule, create }, state = initialState) => {
    onAction(actionTypes.SET, (payload) => {
      state = payload;
    });

    onAction(actionTypes.RESET, (payload) => {
      state = initialState;
      return new Promise(resolve => resolve(null));
    });

    onAction(actionTypes.MOVE_LEFT, () => {
      if (state.position[1] - 1 >= 0) {
        state = { position: [state.position[0], state.position[1] - 1] };
      }
    });

    onAction(actionTypes.MOVE_RIGHT, (groundSize) => {
      if (state.position[1] + 1 < groundSize) {
        state = { position: [state.position[0], state.position[1] + 1] };
      }
    });

    onAction(actionTypes.MOVE_UP, () => {
      if (state.position[0] - 1 >= 0){
        state = { position: [state.position[0] - 1, state.position[1]] };
      }
    });

    onAction(actionTypes.MOVE_DOWN, (groundSize) => {
      if (state.position[0] + 1 < groundSize) {
        state = { position: [state.position[0] + 1, state.position[1]] };
      }
    });

    return state;
  });
