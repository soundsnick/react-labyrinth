import { createAtom } from '@reatom/core';

export enum Stages {
  MENU,
  STARTED,
  LOST
}

export const timeDefault = 60;

type Props = {
  score: number;
  stage: Stages;
};

const initialState: Props = {
  score: 0,
  stage: Stages.MENU,
};

enum actionTypes {
  SET = '_set',
  ADD_SCORE = 'addScore',
  RESET_SCORE = 'resetScore',
  SET_STAGE = 'setStage',
}

const actionCreators = {
  [actionTypes.SET]: (newState: Props) => newState,
  [actionTypes.ADD_SCORE]: (leftSeconds: number, roadLength: number) => ({ leftSeconds, roadLength }),
  [actionTypes.RESET_SCORE]: () => null,
  [actionTypes.SET_STAGE]: (newStage: Stages) => newStage,
};

export const gameAtom = createAtom(
  actionCreators,
  ({ onAction, schedule, create }, state = initialState) => {
    onAction(actionTypes.SET, (payload) => {
      state = payload;
    });

    onAction(actionTypes.ADD_SCORE, ({ leftSeconds, roadLength }) => {
      state = { ...state, score: Math.round(state.score + (leftSeconds + ((roadLength/timeDefault) * 100))) };
    });

    onAction(actionTypes.RESET_SCORE, () => {
      state = { ...state, score: 0 };
    });

    onAction(actionTypes.SET_STAGE, (payload) => {
      state = { ...state, stage: payload };
    });

    return state;
  });
