import { createAtom } from '@reatom/core';

type Score = {
  datetime: Date;
  score: number;
};
type Props = Array<Score>;

const initialState: Props = [];

enum actionTypes {
  SET = '_set',
  RESET = 'reset',
  INIT = 'init',
  PUSH = 'push',
}

const actionCreators = {
  [actionTypes.SET]: (newState: Props) => newState,
  [actionTypes.RESET]: () => null,
  [actionTypes.INIT]: () => null,
  [actionTypes.PUSH]: (score: Score) => score,
};

export const scoreboardAtom = createAtom(
  actionCreators,
  ({ onAction, schedule, create }, state = initialState) => {
    onAction(actionTypes.SET, (payload) => {
      state = payload;
    });

    onAction(actionTypes.RESET, (payload) => {
      state = initialState;
      return new Promise(resolve => resolve(null));
    });

    onAction(actionTypes.INIT, () => {
      const jsonScoreboard = localStorage.getItem('scoreboard');
      if (jsonScoreboard) {
        const scoreboardParsed = JSON.parse(jsonScoreboard);
        if (scoreboardParsed?.length) {
          state = scoreboardParsed;
        }
      }
    });

    onAction(actionTypes.PUSH, (score) => {
      const newScoreboard = [...state, score];
      state = newScoreboard;
      localStorage.setItem('scoreboard', JSON.stringify(newScoreboard));
    });

    return state;
  });
