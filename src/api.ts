import axios from 'axios';

export const api = axios.create({
  baseURL: process.env.REACT_APP_BACKEND,
  headers: {
    ...(localStorage.getItem('token') ? {
      Authorization: `Bearer ${localStorage.getItem('token')}`
    } : {})
  }
});

export const apiControl = (sessionId: string) => axios.create({
  baseURL: process.env.REACT_APP_BACKEND,
  headers: {
    Authorization: `Bearer ${sessionId}`
  }
});
