import { FC, useEffect, useMemo } from 'react';
import { Button, Card, List } from 'antd';
import { useAtom } from '@reatom/react';
import { scoreboardAtom } from '../atoms/scoreboardAtom';
import moment from 'moment';
import { useNavigate } from 'react-router-dom';

export const Scoreboard: FC = () => {
  const [scoreboard, { init }] = useAtom(scoreboardAtom);
  const navigate = useNavigate();

  const bestScore = useMemo(() => {
    let best = 0;
    scoreboard.forEach((n) => {
      if (n.score > best) {
        best = n.score;
      }
    });
    return best;
  }, [scoreboard]);

  useEffect(() => {
    init();
  }, [init]);

  return (
    <Card className={'animate__animated animate__fadeInUp text-center w-100'} style={{ maxWidth: 500 }}>
      <h1 className={'m-0'}>Scoreboard</h1>
      <List
        dataSource={scoreboard}
        header={
          <List.Item>
            <span style={{ fontWeight: 'bold', fontSize: 18 }}>Score</span>
            <p className={'m-0'} style={{ fontWeight: 'bold', fontSize: 18 }}>Date</p>
          </List.Item>
        }
        renderItem={(item, i) => (
          <List.Item key={i} {...(item.score === bestScore ? { style: { fontWeight: 'bold', color: 'darkorange' } } : {})}>
            <span>{item.score} points</span>
            <p className={'m-0'}>{moment(item.datetime).format('DD.MM.YYYY HH:mm')}</p>
          </List.Item>
        )}
      />
      <Button onClick={() => navigate('/')}>Back to game</Button>
    </Card>
  );
};
