import React, { FC, useCallback, useEffect, useRef, useState } from 'react';
import { ControlButton } from '../components/control-button';
import {
  ArrowDownOutlined,
  ArrowLeftOutlined,
  ArrowRightOutlined,
  ArrowUpOutlined,
} from '@ant-design/icons';
import { useParams } from 'react-router-dom';
import { io, Socket } from 'socket.io-client';
import { Button } from 'antd';
import { apiControl } from '../api';

export const Control: FC = () => {
  const socketRef = useRef<Socket | null>(null);
  const params = useParams<{ sessionId: string }>();
  const [error, setError] = useState('');

  useEffect(() => {
    apiControl(params.sessionId ?? 'undefined')
      .get('/api/roadSession')
      .catch(() => {
        setError('Session expired');
      });
  }, [params.sessionId]);

  useEffect(() => {
    socketRef.current = io(process.env.REACT_APP_SOCKET ?? 'localhost:4000');

    return () => {
      socketRef.current?.disconnect();
    }
  }, []);


  const moveUp = useCallback(() => {
    socketRef.current?.emit('ON_MESSAGE', { seanceId: params.sessionId, message: 'up' });
  }, [params, socketRef]);

  const moveDown = useCallback(() => {
    socketRef.current?.emit('ON_MESSAGE', { seanceId: params.sessionId, message: 'down' });
  }, [params, socketRef]);

  const moveLeft = useCallback(() => {
    socketRef.current?.emit('ON_MESSAGE', { seanceId: params.sessionId, message: 'left' });
  }, [params, socketRef]);

  const moveRight = useCallback(() => {
    socketRef.current?.emit('ON_MESSAGE', { seanceId: params.sessionId, message: 'right' });
  }, [params, socketRef]);

  const restart = useCallback(() => {
    socketRef.current?.emit('ON_MESSAGE', { seanceId: params.sessionId, message: 'restart' });
  }, [params, socketRef]);

  return error ? (
    <div className="text-center">
      <span style={{ color: '#fff' }}>{error}</span>
    </div>
  ) : (
    <div className="text-center">
      <div className={'mb-1'}>
        <ControlButton onClick={moveUp}>
          <ArrowUpOutlined />
        </ControlButton>
      </div>
      <div>
        <ControlButton className={'mr-1'} onClick={moveLeft}>
          <ArrowLeftOutlined />
        </ControlButton>
        <ControlButton className={'mr-1'} onClick={moveDown}>
          <ArrowDownOutlined />
        </ControlButton>
        <ControlButton onClick={moveRight}>
          <ArrowRightOutlined />
        </ControlButton>
      </div>
      <div className={'mt-4'}>
        <Button block danger type={'primary'} onClick={restart}>Restart</Button>
      </div>
    </div>
  );
};
