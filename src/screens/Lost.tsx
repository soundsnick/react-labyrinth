import { FC, useCallback, useEffect, useRef } from 'react';
import { Button, Card } from 'antd';
import { useAtom } from '@reatom/react';
import { gameAtom, Stages } from '../atoms/gameAtom';
import { groundAtom } from '../atoms/groundAtom';
import { playerAtom } from '../atoms/playerAtom';
import { useNavigate } from 'react-router-dom';
import { io, Socket } from 'socket.io-client';

export const Lost: FC = () => {
  const [{ score }, { setStage, resetScore }] = useAtom(gameAtom);
  const [, { reset }] = useAtom(playerAtom);
  const [, { loadGround }] = useAtom(groundAtom);

  const navigate = useNavigate();

  const handleRestart = useCallback(() => {
    loadGround(0);
    reset();
    resetScore();
    setStage(Stages.STARTED);
  }, [loadGround, reset, resetScore, setStage]);

  const handleKeydown = useCallback((e: globalThis.KeyboardEvent) => {
    if (e.key === ' ') {
      handleRestart();
    }
  }, [handleRestart]);

  useEffect(() => {
    window.addEventListener('keydown', handleKeydown, false);
    return () => window.removeEventListener('keydown', handleKeydown, false);
  }, [handleKeydown]);

  const socketRef = useRef<Socket | null>(null);

  useEffect(() => {
    socketRef.current = io(process.env.REACT_APP_SOCKET ?? 'localhost:4000');

    socketRef.current.emit('auth', { seanceId: localStorage.getItem('token') });
    socketRef.current.on('ON_MESSAGE', (message) => {
      switch (message) {
        case 'restart':
          handleRestart();
          break;
        default:
          console.log("Unknown action");
          break;
      }
    });

    return () => {
      socketRef.current?.disconnect();
    }
  }, [handleRestart]);

  return (
    <Card className={'animate__animated animate__fadeInUp text-center'}>
      <h1 className={'m-0'}>You lost! You score is {score}</h1>
      <p style={{ fontSize: 12, color: "grey" }}>Press SPACE or button below to restart</p>
      <Button block danger onClick={handleRestart}>Restart</Button>
      <Button className={'mt-2'} block onClick={() => navigate('/scoreboard')}>Scoreboard</Button>
    </Card>
  );
};
