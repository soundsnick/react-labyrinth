import React, {
  FC,
  MutableRefObject,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import { useAtom } from '@reatom/react';
import { Button, Modal } from 'antd';
import QRCode from "react-qr-code";


import { groundAtom } from '../atoms/groundAtom';
import { playerAtom } from '../atoms/playerAtom';
import { gameAtom, Stages, timeDefault } from '../atoms/gameAtom';
import { ControlButton } from '../components/control-button';
import {
  ArrowDownOutlined,
  ArrowLeftOutlined,
  ArrowRightOutlined,
  ArrowUpOutlined,
} from '@ant-design/icons';
import { scoreboardAtom } from '../atoms/scoreboardAtom';
import { useNavigate } from 'react-router-dom';
import { api } from '../api';
import { io, Socket } from 'socket.io-client';

export const Game: FC = () => {
  const [{ stage, score }, { setStage, addScore, resetScore }] = useAtom(gameAtom);
  const [{ road, roadLength, loading, groundSize, level }, { loadGround }] = useAtom(groundAtom);
  const [{ position }, { moveDown, moveLeft, moveRight, moveUp, reset: resetPosition }] = useAtom(playerAtom);
  const [scoreboard, { push: pushScore }] = useAtom(scoreboardAtom);
  const [remoteModalOpen, setRemoveModalOpen] = useState(false);

  const navigate = useNavigate();

  const [timeCount, setTimeCount] = useState(timeDefault);
  const canvasRef = useRef() as MutableRefObject<HTMLCanvasElement>;

  const pxlSize = useMemo(() => 320 / groundSize, [groundSize]);

  const bestScore = useMemo(() => {
    let best = 0;
    scoreboard.forEach((n) => {
      if (n.score > best) {
        best = n.score;
      }
    });
    return best;
  }, [scoreboard]);

  const complexity = useMemo(() => {
    if (level >= 5 && level < 10) {
      return (
        <span
          style={{
            textTransform: 'uppercase',
            color: 'orange'
          }}
        >
          Medium
        </span>
      );
    } else if (level >= 10 && level < 15) {
      return (
        <span
          style={{
            textTransform: 'uppercase',
            color: 'red'
          }}
        >
          Hard
        </span>
      );
    } else if (level >= 15) {
      return (
        <span
          style={{
            textTransform: 'uppercase',
            color: 'red',
            fontWeight: 'bold',
            textShadow: '0 0 15px red'
          }}
        >
          Expert
        </span>
      );
    } else {
      return (
        <span
          style={{
            textTransform: 'uppercase',
            color: 'green',
          }}
        >
          Easy
        </span>
      );
    }
  }, [level]);

  // Canvas render function
  const renderCanvas = useCallback(() => {
    if (canvasRef?.current) {
      const ctx = canvasRef.current.getContext('2d');
      if (ctx) {
        for (let rowIndex = 0; rowIndex < groundSize; rowIndex++) {
          for (let columnIndex = 0; columnIndex < groundSize; columnIndex++) {
            if (position[0] === rowIndex && position[1] === columnIndex) { // Render player position
              ctx.fillStyle = 'blue';
              ctx.fillRect(columnIndex * pxlSize, rowIndex * pxlSize, pxlSize, pxlSize);
            } else if (road[rowIndex]?.[columnIndex] !== undefined) { // Render road and finish
              ctx.fillStyle = road[rowIndex][columnIndex] ? '#11ff31' : '#fff';
              ctx.fillRect(columnIndex * pxlSize, rowIndex * pxlSize, pxlSize, pxlSize);
            } else {
              ctx.fillStyle = '#0b0c11';
              ctx.fillRect(columnIndex * pxlSize, rowIndex * pxlSize, pxlSize, pxlSize);
            }
          }
        }
      }
    }
  }, [groundSize, position, pxlSize, road]);

  // Restart button
  const handleRestart = useCallback((level: number) => {
    setTimeCount(timeDefault);
    resetPosition();
    loadGround(level);
  }, [loadGround, resetPosition]);

  const keyboardMapping: Record<string, Function> = useMemo(() => ({
    ArrowLeft: moveLeft,
    ArrowRight: moveRight,
    ArrowUp: moveUp,
    ArrowDown: moveDown,

    a: moveLeft,
    d: moveRight,
    w: moveUp,
    s: moveDown,

    Escape: () => handleRestart(0)
  }), [handleRestart, moveDown, moveLeft, moveRight, moveUp]);

  // Keyboard adapter
  const handleKeydown = useCallback((e: globalThis.KeyboardEvent) => {
    if (e.key in keyboardMapping) {
      keyboardMapping[e.key](groundSize);
    }
  }, [groundSize, keyboardMapping]);

  const saveScore = useCallback(() => {
    if (score > 0) {
      pushScore({ datetime: new Date(Date.now()), score });
    }
  }, [pushScore, score]);

  // Set timer
  useEffect(() => {
    let timer: NodeJS.Timer;
    if (stage === Stages.STARTED) {
      timer = setInterval(() => {
        setTimeCount(prev => prev - 1);
      }, 1000);
    }

    return () => clearInterval(timer);
  }, [stage]);

  // Render canvas
  useEffect(() => {
    renderCanvas();
  }, [position, renderCanvas]);

  // Check new position
  useEffect(() => {
    if (position && Object.keys(road).length) {
      const [rowIndex, columnIndex] = position;
      const item = road[rowIndex]?.[columnIndex];
      if (item !== undefined) {
        if (item) {
          addScore(timeCount, roadLength);
          handleRestart(level + 1);
        }
      } else {
        saveScore();
        setStage(Stages.LOST);
      }
    }
  }, [addScore, road, position, setStage, resetScore, handleRestart, timeCount, roadLength, level, pushScore, score, saveScore]);

  // Timer end restart
  useEffect(() => {
    if (timeCount <= 0) {
      saveScore();
      setStage(Stages.LOST);
    }
  }, [pushScore, saveScore, score, setStage, timeCount]);

  // Add keydown listeners
  useEffect(() => {
    window.addEventListener('keydown', handleKeydown, false);

    return () => window.removeEventListener('keydown', handleKeydown, false);
  }, [handleKeydown, moveDown, moveLeft, moveRight, moveUp]);

  useEffect(() => {
    api.get(`/api/roadSession`)
      .catch((e) => {
        if (e.response.status === 401) {
          api.post('/api/roadSession')
            .then((res) => {
              localStorage.setItem('token', res.data.token);
              window.location.reload();
            });
        }
      });
  }, []);

  const socketRef = useRef<Socket | null>(null);

  useEffect(() => {
    socketRef.current = io(process.env.REACT_APP_SOCKET ?? 'localhost:4000');

    socketRef.current.emit('auth', { seanceId: localStorage.getItem('token') });
    socketRef.current.on('ON_MESSAGE', (message) => {
      switch (message) {
        case 'up':
          moveUp();
          break;
        case 'down':
          moveDown(groundSize);
          break;
        case 'left':
          moveLeft();
          break;
        case 'right':
          moveRight(groundSize);
          break;
        case 'restart':
          handleRestart(0);
          break;
        default:
          console.log("Unknown action");
          break;
      }
    });

    return () => {
      socketRef.current?.disconnect();
    }
  }, [groundSize, handleRestart, moveDown, moveLeft, moveRight, moveUp]);

  return loading ? <span>Loading a stage...</span> : (
    <div style={{ alignSelf: 'start', paddingTop: 30 }}>
      <div className="d-flex align-items-center justify-content-between my-3 position-relative" style={{ color: '#fff' }}>
        <div onClick={() => navigate('/scoreboard')}>
          <p className={'m-0'}>Score: {score}</p>
          <p className={'m-0'}>Best Score: {bestScore}</p>
        </div>
        <div
          style={{
            position: 'absolute',
            left: '50%',
            transform: 'translateX(-50%)',
            textAlign: 'center'
          }}
        >
          <p
            className={'m-0'}
            style={{
              fontWeight: 'bold',
            }}
          >
            {timeCount}
          </p>
          {complexity}
        </div>

        <span>Length: {roadLength}</span>
      </div>
      <div className="text-center">
        <canvas
          className={'animate__animated animate__fadeIn'}
          ref={canvasRef}
          width={320}
          height={320}
          style={{
            backgroundColor: '#0b0c11'
          }}
        />
      </div>
      <div className="text-center my-3">
        <Button
          danger
          type={'primary'}
          onClick={() => handleRestart(0)}
        >Restart (Esc)</Button>
        <Button onClick={() => setRemoveModalOpen(true)}>Remote control</Button>
      </div>
      <div className="text-center">
        <div className={'mb-1'}>
          <ControlButton onClick={moveUp}>
            <ArrowUpOutlined />
          </ControlButton>
        </div>
        <div>
          <ControlButton className={'mr-1'} onClick={moveLeft}>
            <ArrowLeftOutlined />
          </ControlButton>
          <ControlButton className={'mr-1'} onClick={() => moveDown(groundSize)}>
            <ArrowDownOutlined />
          </ControlButton>
          <ControlButton onClick={() => moveRight(groundSize)}>
            <ArrowRightOutlined />
          </ControlButton>
        </div>
      </div>
      <Modal
        visible={remoteModalOpen}
        cancelButtonProps={{ disabled: true, hidden: true }}
        okText={'Close'}
        onOk={() => setRemoveModalOpen(false)}
      >
        <div className="text-center">
          <p>Scan this QR</p>
          <QRCode value={`https://game.asqaruly.com/control/${localStorage.getItem('token')}`} />
        </div>
      </Modal>
    </div>
  );
};


