import { FC, useCallback, useEffect } from 'react';
import { Button, Card } from 'antd';
import { useAtom } from '@reatom/react';
import { groundAtom } from '../atoms/groundAtom';
import { gameAtom, Stages } from '../atoms/gameAtom';
import { useNavigate } from 'react-router-dom';

export const Menu: FC = () => {
  const [, { loadGround }] = useAtom(groundAtom);
  const [, { setStage }] = useAtom(gameAtom);

  const navigate = useNavigate();

  const handleStart = useCallback(() => {
    loadGround(0);
    setStage(Stages.STARTED);
  }, [loadGround, setStage]);

  const handleKeydown = useCallback((e: globalThis.KeyboardEvent) => {
    if (e.key === ' ') {
      handleStart();
    }
  }, [handleStart]);

  useEffect(() => {
    window.addEventListener('keydown', handleKeydown, false);
    return () => window.removeEventListener('keydown', handleKeydown, false);
  }, [handleKeydown]);

  return (
    <Card className={'animate__animated animate__fadeInUp text-center'}>
      <span>Controls</span>
      <div className="text-center">
        <img src={'/wasd.webp'} style={{ display: 'block', height: 150 }} />
      </div>

      <Button type={'primary'} block onClick={handleStart}>Start</Button>
      <Button className={'mt-2'} block onClick={() => navigate('/scoreboard')}>Scoreboard</Button>
    </Card>
  );
};
